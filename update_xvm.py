#!/usr/bin/env python3
"""
Download the specified XVM version and apply it to the "xvm" branch.  You need
to commit the changes manually and merge them to "master".

Usage:
  ./update_xvm.py XVM_VERSION

"""
import fnmatch
import functools
import os
import subprocess
import sys

import bs4
import requests


WOT_VER = sorted(fnmatch.filter(os.listdir('mods/'), '1.*'))[-1]
XVM_URL = 'https://modxvm.com/en/download-xvm/'


response = requests.get(XVM_URL)
soup = bs4.BeautifulSoup(response.text, 'html.parser')
div = soup.find('div', id='content').find('div', id='primary')
row = soup.find('tr', class_='cursor-pointer')
xvm_download = row.find('td').text
_, xvm_ver, *_, wot_compat = xvm_download.split()

if wot_compat != WOT_VER:
    print(
        f'XVM {xvm_ver} is compatible with WoT {wot_compat} but WoT {WOT_VER} '
        f'is installed'
    )
    exit()

assert wot_compat == WOT_VER
answer = input(
    f'Update WoT {WOT_VER} to XVM {xvm_ver}? [Y/n]: '
)
if answer == 'n':
    exit()

run = functools.partial(subprocess.run, shell=True, check=True)

run('git checkout xvm')

# Delete old mod so that we can better detect new files
run('rm readme-*.txt')
run('git rm -rf mods/1.*/com.modxvm.xfw')
run('rm -rf res_mods/configs/xvm')
run('rm -rf res_mods/mods/shared_resources/xvm')
run('rm -rf res_mods/mods/xfw_packages')

# Download mod and store changes to config for later comparision
# run(
#     f'curl --referer http://www.modxvm.com/en/download-xvm/ -O '
#     f'http://dl1.modxvm.com/bin/xvm-{xvm_ver}.zip'
# )
run(
    f'http --download --output=xvm-{xvm_ver}.zip '
    f'https://dl1.modxvm.com/bin/xvm-{xvm_ver}.zip '
    f'Referer:https://www.modxvm.com/en/download-xvm/'
)
run(f'unzip -o xvm-{xvm_ver}.zip')
run(f'git diff --patience res_mods/configs/xvm > xvm.diff')

# Add new files
run(f'git add -u')
run(f'git add mods/{WOT_VER}/com.modxvm.xfw')
run(f'git add res_mods/configs/xvm')
run(f'git add res_mods/mods/shared_resources/xvm')
run(f'git add res_mods/mods/xfw_packages')

# Commit and merge changes
run('git status')
commit = f"git ci -m 'Update to XVM {xvm_ver}'"
answer = input(f'Commit changes? [Y/n] ')
if answer == 'n':
    print(f'Please commit when you are done:\n{commit}')
    exit()
run(commit)
run('git checkout master')
run('git merge xvm', check=False)
run('git rm -rf res_mods/configs/xvm/default', check=False)
run('git rm -rf res_mods/configs/xvm/sirmax', check=False)
run('git status')
