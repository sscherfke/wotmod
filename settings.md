WoT Settings
============

Settings for World of Tanks


General
-------

### Chat

- [ ] Censor offensive words in chat messages
- [ ] Hide spam
- [ ] Display date in messages
- [x] Display time in messages
- [ ] Accept invitations from friends in Garage
- [x] Accept invitations to Platoon in battle
- [x] Accept friend requests
- [ ] Receive messages only from the Contact list
- [ ] Disable chat with allies
- [x] Receive clan notifications

### Random Battle Types

- [x] Standard Battle
- [x] Encounter Battle
- [x] Assault
- [x] Grand Battle

### Vehicle panel

- Two-level
- Two-level vehicle panel: Adaptive
- [x] Display stats upon hovering over a slot in the Garage

### Battle Interface

- [x] Show vehicle tier
- [ ] Minimap Transparency
- [x] Display Dog Tags of enemies you destroy
- [x] Display your actual Dog Tag
- [x] Enable optics effect in Sniper mode
- [x] Show Postmortem mode vehicle that destroyed your vehicle
- [ ] Enable dynamic camera
- [x] Horizontal stabilization in Sniper mode
- [x] Enable the x16 and x25
- [ ] Restrict toggling the Sniper mode to the Shift key
- [ ] Enable server reticle
- Preferred Sniper Mode Zoom Level: Last Used
- [x] Show vehicle markers on Battle Score Panel
- [x] Display camera direction beam on the minimap
- [x] Display the arc of fire for the SPG on the minimap
- [x] Display notifications when causing damage
- [x] Display speedometer of wheeled vehicles
- [x] Show the repair time for damaged modules
- [x] Show previous battle results in chat
- Enable Expanded Minimap Features: Always
- Tips on the Battle Loading Screen: Minimap
- Tips on the Ranked Battle loading screen: Minimap

### View range inidicators on the Minimap

- [x] Show the proximity spotting circle
- [x] Show the view range circle
- [x] Show the maximum spotting range
- [x] Show the draw circle

### Garage camera animation

- Garage camera rotation when idle for: 30 seconds
- [ ] Garage camera animation when moving the mouse cursor

### Misc.

- Enable Battle Recording: All
- [x] Display Marks of Excellence
- [x] Hide non-historical elements
- [x] Display vehicles of Platoon members in the Garage
- [ ] Display server selection upon game launch
- [ ] Anonymize your nickname in battle


Graphics
--------

- 3D Render Resolution: 100
- [ ] Dynamic Adjustment

### Screen

### Details

- Graphics: Ultra
- [ ] Grass in Sniper Mode
- [x] *Everything else*


Sound
-----

- Master Volume: 30

### Volume

- Interface: 50
- Vehicles: 50
- Voice Notifications: 50
- Effects: 50
- Ambience: 100
- Music in Battle: 0
- Music in Garage: 0

### Presets

- [ ] Night Mode
- [ ] Low quality
- [ ] Low frequency enhancement
- [ ] Subtitles
- Headphones

### Sound Environment Elements

- Voice Messages in Battle: Commander
- Sixth Sense Activation Sound: Variant 1


Reticle
-------

### Arcade

- Indicator: Diagonal: 90
- Central Marker: Aimpoint I: 90
- Load: 90
- Condition: 90
- Aiming: Circle I (with load indicator): 90
- Gun Marker: O-shaped I (with armor penetration indicator): 90
- Containers: 90
- Loading Timer: 100

### Sniper

- Indicator: Diagonal: 90
- Central Marker: Aimpoint I: 90
- Load: 90
- Condition: 90
- Aiming: Circle I (with load indicator): 90
- Gun Marker: O-shaped I (with armor penetration indicator): 90
- Containers: 90
- Loading Timer: 100
- Zoom Indicator: 100


Marker
------

### Enemy Team

#### Default

- [ ] Vehicle icon
- [ ] Tier
- [x] Vehicle model
- [ ] Player's name
- [x] Health indicator
- Vehicle Health: HP left / total
- [x] Damage received

#### Alt

- [x] Vehicle icon
- [x] Tier
- [x] Vehicle model
- [x] Player's name
- [x] Health indicator
- Vehicle Health: HP left / total
- [x] Damage received

### Our Team

#### Default

- [ ] Vehicle icon
- [ ] Tier
- [x] Vehicle model
- [x] Player's name
- [x] Health indicator
- Vehicle Health: HP left / total
- [x] Damage received

#### Alt

- [x] Vehicle icon
- [x] Tier
- [x] Vehicle model
- [x] Player's name
- [x] Health indicator
- Vehicle Health: HP left / total
- [x] Damage received

### Detroyed

#### Default

- [ ] Vehicle icon
- [ ] Tier
- [x] Vehicle model
- [x] Player's name
- [ ] Health indicator
- Vehicle Health: Hide
- [x] Damage received

#### Alt

- [x] Vehicle icon
- [x] Tier
- [x] Vehicle model
- [x] Player's name
- [x] Health indicator
- Vehicle Health: HP left / total
- [x] Damage received


Battle Notifications
--------------------

### Fire Direction Indicator

- View: Advanced
- Display in battle:
  - [x] Critical hits
  - [x] Friendly fire
- Information:
  - [x] Amount of damage
  - [x] Dynamic indicators
  - [x] Vehicle info
  - [x] Display animation

### Battle Events

*All checked*

### Log

- Summarized damage: *all*
- Detailed damage: Display always
- Event type: All events
- Position of events: All events at the bottom

### Map Border

- Border Type: Wall
- Additional Highlighting: When approaching

### Battle Report & Missions

- Battle Score:
  - [x] Display team HP bar
  - [ ] Display team HP number
  - [ ] Display team HP difference
  - [ ] Display vehicle icons by tier
- Display missions: Standard
- Missions Details: Display all
