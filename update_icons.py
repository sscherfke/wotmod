#!/usr/bin/env python3
"""
Extract original WoT icons and colorize them.

The script currently can't generate the required DDS files.  It generates PNGs
instead that you have to convert to DDS manually.  You can use Paint.net with
the default DTX5 settings.

Requires:

- Python 3
- Pillow

Usage::

    $ ./make_icons.py PATH_TO_WOT

"""
import fnmatch
import io
import os
import pathlib
import shutil
import sys
import xml.etree.ElementTree as ET
import zipfile

from PIL import Image, ImageDraw, ImageOps

sys.path.insert(0, "res_mods")
import bxml


# Options
COLORS = {
    "lightTank": "#917D41",
    "mediumTank": "#71855D",
    "heavyTank": "#808080",
    "AT-SPG": "#6D7F91",
    "SPG": "#996F6B",
}

# WoT internals
WOT_VER = sorted(fnmatch.filter(os.listdir("mods/"), "1.*"))[-1]
OUT_DIR = pathlib.Path("res_mods") / WOT_VER
GUI_FILE = pathlib.Path("res/packages/gui-part1.pkg")
SCRIPTS_FILE = pathlib.Path("res/packages/scripts.pkg")
VEHICLE_LIST_GLOB = "scripts/item_defs/vehicles/*/list.xml"
ATLASES_PATH = "gui/flash/atlases"
ATLASES = [
    ("battleAtlas.dds", "battleAtlas.png", "battleAtlas.xml"),
    ("vehicleMarkerAtlas.dds", "vehicleMarkerAtlas.png", "vehicleMarkerAtlas.xml"),
]


def extract_vehicle_data(scripts_zip: zipfile.ZipFile) -> dict[str, dict[str, object]]:
    """
    Read and return the vehicle data from the open zip file *scripts_zip*.
    """
    names = [
        pathlib.PurePosixPath(n)
        for n in scripts_zip.namelist()
        if fnmatch.fnmatch(n, VEHICLE_LIST_GLOB)
    ]
    vehicles = {}
    for name in names:
        country = name.parent.stem
        with scripts_zip.open(str(name)) as f:
            data = bxml.read_file(f)
            for k, v in data.items():
                vehicles[f"{country}-{k}"] = v

    return vehicles


def make_atlases(
    vehicles: dict[str, dict[str, object]],
    gui_zip: zipfile.ZipFile,
    outdir: pathlib.Path,
) -> None:
    """Extract the atlas files from the game and update them with the new
    icons."""
    # Clean dest dir
    dest_dir = OUT_DIR / ATLASES_PATH
    shutil.rmtree(dest_dir, ignore_errors=True)
    dest_dir.mkdir(parents=True, exist_ok=True)

    for adds, apng, axml in ATLASES:
        apng = f"{ATLASES_PATH}/{apng}"
        adds = f"{ATLASES_PATH}/{adds}"
        axml = f"{ATLASES_PATH}/{axml}"
        gui_zip.extract(axml, outdir)

        # Generate LUT "vehicle : (x, y, width, height)"
        lut = {}
        with gui_zip.open(axml) as f:
            root = ET.fromstring(f.read())
            for child in root:
                lut[child.find("name").text.strip()] = (
                    int(child.find("x").text),
                    int(child.find("y").text),
                    int(child.find("width").text),
                    int(child.find("height").text),
                )

        # Load atlas image
        with gui_zip.open(adds) as f:
            data = io.BytesIO(f.read())
            img = Image.open(data)
        img.load()

        # Extract tank icons, colorize them, and paste them back
        for tank, (x, y, w, h) in lut.items():
            try:
                info = vehicles[tank]
            except KeyError:
                continue

            tags = info["tags"]
            for vtype in COLORS:
                if vtype in tags:
                    color = COLORS[vtype]
                    break

            icon = img.crop((x, y, x + w, y + h))
            icon = tint_image(icon, color)
            img.paste(icon, (x, y))

        # Write result as png
        img.save(outdir / apng)


def tint_image(src: Image, color: str) -> Image:
    """Colorize the *src* :class:`PIL.Image` with *color* (`'#RRGGBB'`)."""
    src.load()
    if src.mode == "P":
        src = src.convert("RGBA")
    alpha = src.getchannel("A")
    gray = ImageOps.grayscale(src)
    result = ImageOps.colorize(gray, black="#000000", white="#FFFFFF", mid=color)
    result.putalpha(alpha)
    return result


def main():
    wot_dir = pathlib.Path(sys.argv[1] if len(sys.argv) > 1 else ".")

    with zipfile.ZipFile(wot_dir / SCRIPTS_FILE) as scripts_zip:
        vehicles = extract_vehicle_data(scripts_zip)

    with zipfile.ZipFile(wot_dir / GUI_FILE) as gui_zip:
        # icon_paths = get_icon_paths(gui_zip, vehicles)
        # icons = make_icons(icon_paths, vehicles, gui_zip)
        make_atlases(vehicles, gui_zip, OUT_DIR)


if __name__ == "__main__":
    main()
