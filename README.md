Stefan’s WoT Modpack
====================

This is a minimalistic modpack with [XVM](http://www.modxvm.com/en/).
You are welcome to fork this mod and mofiy the XVM config as you like.

Installation
------------

This modpack doesn’t come with a shiny installer.
You can either [download the latest version](https://gitlab.com/sscherfke/wotmod/repository/master/archive.zip) and extrated it into your `World_of_Tanks` folder or clone this repo onto your machine.
This way, you can easily pull in the latest changes and keep your modifications at the same time.


XVM
---

### Login

- Auto-login
- Save last server


### Hangar

- Automatically return crew (optionally)
- Block tanks with ≤ 20% ammo
- Show server ping
- Display hangar clock
- Carousel shows for each tank:
  - Left: tank tier, win rage (w/o coloring), XP required to elite tank (everything is researched)
  - Right: daily bonus, mastery sign, gun marks, marks of excellence (in percent, w/o coloring)
  - Examples in comments: tank tier, battle tiers, win rate, played battles, average damage, xTE, XP required to elite tank (everything is researched)

![Hangar](res_mods/mod_xvm_hangar.png)


### Sounds

- Disable all XVM sounds


### Colors

- Make default colors a bit more comfortable to look at …

  - `#E64545`
  - `#E68250`
  - `#E6CB45`
  - `#5FB336`
  - `#4C92D9`
  - `#A667E6`

- … but disable most colors anyway.

- “Fixed” color scale for *damageRating* (gun marks in percents).
  *normal* is now at 65% (one mark), *good* at 85% (two marks) and *very good* at 95% (three marks).


### Minimap

Enhanced vanilla minimap using similar colors

- Labels:
  - Show vehicle names
  - Show icons and vehicle names for destroyed tanks

- Circles:
  - 50m proximity spotting circle (cyan)
  - 445m maximum spotting range circle (white)
  - 564m maximum drawing distance circle (yellow)
  - Current (dynamic) view range circle (green)
  - view range circle while standing (with binocs) (dark green)
  - Max. shooting range for low caliber machine guns and artillery (red)

- Lines:
  - Camera direction with dots every 100m (orange)
  - Gun traverse angle (if applicable) (light gray)

![Normal minimap; TD with binocs and gun arcs displayed; Low caliber machine gun](res_mods/mod_xvm_minimap.png)


### Battle loading / statistics panel

- Don’t show any XVM ratings and colors.


### Players Panel

- Don’t show any XVM ratings and colors.

- Colorized tank icons:

    - Light tanks: yellow
    - Medum tanks: green
    - Heavy tanks: gray
    - TDs: blue
    - SPGs: red

    There is a [Python script](update_icons.py) that you can use to generate tank icons with the colors you like.

- XVM’s spotted markers for enemy tanks.
  They are easier to see than the built-in ones and thus less distracting.

- Show HP bars when `ALT` is pressed.

![Medium panel with HP bars and spoted markers for enemies](res_mods/mod_xvm_playerspanel.png)


Sources
-------

- [XVM](http://www.modxvm.com/en/)
