﻿/**
 * Parameters for login screen.
 */
{
  "login": {
    // Auto enter to the game.
    //  - Option to enable/disable automatic login to the game when the WGC login-manager is not available;
    //    Otherwise, client login is controlled by an option in the client "Display server selection upon game launch".
    "autologin": true,
    // Auto confirm old replays playing.
    "confirmOldReplays": false,
    // Do not show the specified servers in the servers list, for example, ["RU1", "RU3"].
    "disabledServers": [],
    // Ping servers.
    "pingServers": {
        "enabled": false
    },
    "onlineServers": {
        "enabled": false
    },
    // Parameters for widgets.
    "widgets": ${"widgets.xc":"widgets.login"}
  }
}
