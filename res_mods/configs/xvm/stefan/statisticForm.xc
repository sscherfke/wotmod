﻿/**
 * Parameters of the Battle Statistics form.
 */
{
  "statisticForm": {
    // true - disable platoon/rank icons.
    "removeSquadIcon": false,
    // true - disable badge icons.
    "removeRankBadgeIcon": false,
    // true - disable alpha/beta testers icons.
    "removeTesterIcon": false,
    // Opacity percentage of vehicle icon. 0 - transparent ... 100 - opaque.
    "vehicleIconAlpha": 100,
    // true - disable vehicle level indicator.
    "removeVehicleLevel": false,
    // true - disable vehicle type icon. This space will be used for formatted vehicle field.
    "removeVehicleTypeIcon": false,
    // true - disable player status icon.
    "removePlayerStatusIcon": false,
    // Show border for name field (useful for config tuning).
    "nameFieldShowBorder": false,
    // Show border for vehicle field (useful for config tuning).
    "vehicleFieldShowBorder": false,
    // Show border for frags field (useful for config tuning).
    "fragsFieldShowBorder": false,
    // X offset for allies squad icons.
    "squadIconOffsetXLeft": 0,
    // X offset for enemies squad icons.
    "squadIconOffsetXRight": 0,
    // X offset for allies player names field.
    "nameFieldOffsetXLeft": 0,
    // X offset for enemies player names field.
    "nameFieldOffsetXRight": 0,
    // Width of allies player names field.
    "nameFieldWidthLeft": 200,
    // Width of enemies names field.
    "nameFieldWidthRight": 200,
    // X offset for "formatLeftVehicle" field.
    "vehicleFieldOffsetXLeft": 19,
    // X offset for "formatRightVehicle" field.
    "vehicleFieldOffsetXRight": 0,
    // Width of "formatLeftVehicle" field.
    "vehicleFieldWidthLeft": 250,
    // Width of "formatRightVehicle" field.
    "vehicleFieldWidthRight": 250,
    // X offset for allies vehicle icons.
    "vehicleIconOffsetXLeft": 0,
    // X offset for enemies vehicle icons.
    "vehicleIconOffsetXRight": 0,
    // X offset for allies frags.
    "fragsFieldOffsetXLeft": 0,
    // X offset for enemies frags.
    "fragsFieldOffsetXRight": 0,
    // Width of frags field for allies.
    "fragsFieldWidthLeft": 30,
    // Width of frags field for enemies.
    "fragsFieldWidthRight": 30,
    // Display format for the left panel (macros allowed, see macros.txt).
    "formatLeftNick": "<img src='xvm://res/icons/flags/{{flag|default}}.png' width='16' height='13'> <font alpha='{{alive?#FF|#80}}'>{{name%.13s~..}} <font face='mono' size='10'>{{clan}}</font></font>",
    // Display format for the right panel (macros allowed, see macros.txt).
    "formatRightNick": "<font alpha='{{alive?#FF|#80}}'><font face='mono' size='10'>{{clan}}</font> {{name%.13s~..}}</font> <img src='xvm://res/icons/flags/{{flag|default}}.png' width='16' height='13'>",
    // Display format for the left panel (macros allowed, see macros.txt).
    "formatLeftVehicle": "{{vehicle}}",
    // Display format for the right panel (macros allowed, see macros.txt).
    "formatRightVehicle": "{{vehicle}}",
    // Display format for the left panel (macros allowed, see macros.txt).
    "formatLeftFrags": "{{frags}}",
    // Display format for the right panel (macros allowed, see macros.txt).
    "formatRightFrags": "{{frags}}",
    // Extra fields. Fields are placed one above the other.
    // Set of formats for left panel (extended format supported, see extra-field.txt).
    "extraFieldsLeft": [],
    // Set of formats for right panel (extended format supported, see extra-field.txt).
    "extraFieldsRight": []
  }
}
