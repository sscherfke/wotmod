﻿/**
 * Minimap lines. Only for owned vehicle.
 */
{
    "def": {
        // Tank alignment.
        "vehicle": { "enabled": true, "inmeters": true, "color": "0xFFFFFF" },
        // Camera direction definition.
        "camera": { "enabled": true, "inmeters": true, "color": "0x99FF55" },
        // Dots definition.
        "dot": { "enabled": true, "inmeters": true, "color": "0xFFAA00" },
        // Horizontal gun traverse angle definition.
        // Gun traverse angles may differ depending on vehicle angle relative to ground.
        // See pics at: https://kr.cm/f/t/2076/c/35697/
        "traverseAngle": { "enabled": true, "inmeters": true, "color": "0xCCCCCC" }
    },
    "lines": {
        // Distance between farthest corners at 1km map is somewhat more than 1400 meters.
        // Sections can contain any number of lines.
        // To set a point try setting line with length of one and large thickness.
        // You can leave one line for simplicity. Remember comma positioning rules.
        //---------------------------------------------------------------------------------
        // Own vehicle direction.
        "vehicle": [
            // { "$ref": { "path": "def.vehicle" }, "from": 0, "to": 2000, "thickness": 0.5, "alpha": 60 }
        ],
        // Camera direction.
        "camera": [
            { "$ref": { "path": "def.camera" }, "from":    0, "to": 2000, "thickness": 0.5, "alpha": 60 }
            // { "$ref": { "path": "def.camera" }, "from":    0, "to":   80, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from":  120, "to":  180, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from":  220, "to":  280, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from":  320, "to":  380, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from":  420, "to":  480, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from":  520, "to":  580, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from":  620, "to":  680, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from":  720, "to":  780, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from":  820, "to":  880, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from":  920, "to":  980, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from": 1020, "to": 1080, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from": 1120, "to": 1180, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from": 1220, "to": 1280, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from": 1320, "to": 1380, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from": 1420, "to": 1480, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from": 1520, "to": 1580, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from": 1620, "to": 1680, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from": 1720, "to": 1780, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from": 1820, "to": 1880, "thickness": 0.5, "alpha": 60 },
            // { "$ref": { "path": "def.camera" }, "from": 1920, "to": 2000, "thickness": 0.5, "alpha": 60 },
            //Dots
            // { "$ref": { "path": "def.dot" }, "from":   99.99, "to":  100, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from":  199.99, "to":  200, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from":  299.99, "to":  300, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from":  399.99, "to":  400, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from":  499.99, "to":  500, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from":  599.99, "to":  600, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from":  699.99, "to":  700, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from":  799.99, "to":  800, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from":  899.99, "to":  900, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from":  999.99, "to": 1000, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from": 1099.99, "to": 1100, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from": 1199.99, "to": 1200, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from": 1299.99, "to": 1300, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from": 1399.99, "to": 1400, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from": 1499.99, "to": 1500, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from": 1599.99, "to": 1600, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from": 1699.99, "to": 1700, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from": 1799.99, "to": 1800, "thickness": 1.0, "alpha": 60 },
            // { "$ref": { "path": "def.dot" }, "from": 1899.99, "to": 1900, "thickness": 1.0, "alpha": 60 }
        ],
        // Gun traverse angles may differ depending on vehicle angle relative to ground.
        //------------------------------------------------------------------------------
        // Horizontal gun traverse angle lines.
        "traverseAngle": [
            { "$ref": { "path": "def.traverseAngle" }, "from": 50, "to": 2000, "thickness": 0.5, "alpha": 40 }
        ]
    }
}
