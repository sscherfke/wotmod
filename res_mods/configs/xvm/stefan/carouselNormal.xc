﻿/**
 * Normal carousel cells settings.
 */
{
  // Definitions.
  "def": {
    // Text fields shadow.
    "textFieldShadow": { "enabled": true, "color": "0x000000", "alpha": 80, "blur": 2, "strength": 2, "distance": 0, "angle": 0 }
  },
  "normal": {
    // Cell width.
    "width": 160,
    // Cell height.
    "height": 100,
    // Spacing between carousel cells.
    "gap": 10,
    // Standard cell elements.
    "fields": {
      // "enabled"  - the visibility of the element
      // "dx"       - horizontal shift
      // "dy"       - vertical shift
      // "alpha"    - transparency
      // "scale"    - scale
      //
      // Nation flag.
      "flag": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // Vehicle icon.
      "tankIcon": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // Vehicle class icon.
      "tankType": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // Vehicle level.
      "level": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // Double XP icon.
      "xp": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // Vehicle name.
      "tankName": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1, "textFormat": {}, "shadow": {} },
      // Vehicle rent info text.
      "rentInfo": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1, "textFormat": {}, "shadow": {} },
      // Info text (Crew incomplete, Repairs required).
      "info": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1, "textFormat": {}, "shadow": {} },
      // Info image.
      "infoImg": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // Info text for "Buy vehicle" and "Buy slot" slots.
      "infoBuy": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1, "textFormat": {}, "shadow": {} },
      // Clan lock timer.
      "clanLock": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100 },
      // Price.
      "price": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100 },
      // Action price.
      "actionPrice": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100 },
      // Favorite vehicle mark.
      "favorite": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100 },
      // Vehicle mark with the ability to earn bonds.
      "crystalsBorder": { "enabled": true, "alpha": 100 },
      // Image in in the stats field "stats" for vehicle with the ability to earn bonds.
      "crystalsIcon": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100 },
      // Stats field that appears on the mouse hover.
      "stats": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "textFormat": {}, "shadow": {} },
      // Battle Pass progression points.
      "progressionPoints": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 }
    },
    // Extra cell fields (extended format supported, see extra-field.txt).
    "extraFields": [
      // Slot background.
      { "x": 1, "y": 1, "layer": "substrate", "width": 160, "height": 100, "bgColor": "0x0A0A0A" },
      // // Battle tiers.
      // {
      //   "enabled": true,
      //   "x": 41, "y": 0,
      //   "format": "<b><font size='11' color='#C8C8B5'>{{v.battletiermin}}–{{v.battletiermax}}</font></b>",
      //   "shadow": { "color": "0x000000", "alpha": 80, "blur": 2, "strength": 3, "distance": 0, "angle": 0 }
      // },
      // Sign of mastery.
      {
        "enabled": true,
        "x": 160, "y": 20, "width": 24, "height": 24, "align": "right",
        "src": "{{v.mastery!=0?img://gui/maps/icons/library/proficiency/class_icons_{{v.mastery}}.png}}"
      },
      // Gun marks.
      {
        "enabled": true,
        "x": 160, "y": 42, "width": 20, "height": 20, "align": "right", "alpha": "{{v.marksOnGun?|0}}",
        "src": "img://gui/maps/icons/library/marksOnGun/mark_{{v.marksOnGun}}.png"
      },
      // Gun marks (percent).
      {
        "enabled": true,
        "x": 159, "y": 60, "align": "right",
        // "format": "<b><font face='$FieldFont' size='12' color='{{v.c_damageRating}}'>{{v.damageRating%4.2f~%}}</font></b>",
        "format": "<b><font face='$FieldFont' size='12' color='#CFCFCF'>{{v.damageRating%4.2f~%}}</font></b>",
        "shadow": ${ "def.textFieldShadow" }
      },
      // Win rate.
      {
        "enabled": true,
        "x": 2, "y": 18, "width": 13, "height": 13, "alpha": "{{v.winrate?|0}}",
        "src": "xvm://res/icons/carousel/wins.png"
      },
      {
        "enabled": true,
        "x": 17, "y": 15,
        // "format": "<b><font face='$FieldFont' size='12' color='{{v.c_winrate|#CFCFCF}}'>{{v.winrate%2d~%}}</font></b>",
        "format": "<b><font face='$FieldFont' size='12' color='#CFCFCF'>{{v.winrate%2d~%}}</font></b>",
        "shadow": ${ "def.textFieldShadow" }
      },
      // // Battles count.
      // {
      //   "enabled": true,
      //   "x": 2, "y": 33, "width": 13, "height": 13, "alpha": "{{v.battles?|0}}",
      //   "src": "xvm://res/icons/carousel/battles.png"
      // },
      // {
      //   "enabled": true,
      //   "x": 17, "y": 30,
      //   // "format": "<b><font face='$FieldFont' size='12' color='{{v.c_battles||#CFCFCF}}'>{{v.battles}}</font></b>",
      //   "format": "<b><font face='$FieldFont' size='12' color='#CFCFCF'>{{v.battles}}</font></b>",
      //   "shadow": ${ "def.textFieldShadow" }
      // },
      // // Average damage.
      // {
      //   "enabled": true,
      //   "x": -1, "y": 45, "width": 18, "height": 18, "alpha": "{{v.tdb?|0}}",
      //   "src": "xvm://res/icons/carousel/damage.png"
      // },
      // {
      //   "enabled": true,
      //   "x": 17, "y": 45,
      //   "format": "<b><font face='$FieldFont' size='12' color='{{v.c_tdb|#CFCFCF}}'>{{v.tdb%d}}</font></b>",
      //   "shadow": ${ "def.textFieldShadow" }
      // },
      // // Vehicle xTE.
      // {
      //   "enabled": true,
      //   "x": 2, "y": 60, "alpha": "{{v.xte?|0}}",
      //   // There's no icon for this.  Use the xvmsymbol font for this.
      //   "format": "<font face='xvm' size='13' color='#CCCCCC'>P</font>"
      // },
      // {
      //   "enabled": true,
      //   "x": 17, "y": 60,
      //   "format": "<b><font face='$FieldFont' size='12' color='{{v.c_xte|#CFCFCF}}'>{{v.xte}}</font></b>",
      //   "shadow": ${ "def.textFieldShadow" }
      // },
      // XP required to elite vehicle.
      {
        "enabled": true,
        // "x": 2, "y": 78,
        "x": 2, "y": 33,
        "width": 13, "height": 13, "alpha": "{{v.xpToEliteLeft?|0}}",
        "src": "xvm://res/icons/carousel/filter/elite.png"
      },
      {
        "enabled": true,
        // "x": 17, "y": 75,
        "x": 17, "y": 30,
        "format": "<b><font face='$FieldFont' size='12' color='#CFCFCF'>{{v.xpToEliteLeft}}</font></b>",
        "shadow": ${ "def.textFieldShadow" }
      }
    ]
  }
}
