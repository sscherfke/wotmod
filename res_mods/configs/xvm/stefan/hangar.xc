﻿/**
 * Parameters for hangar.
 */
{
  "hangar": {
    // true - show "Buy premium" button.
    "showBuyPremiumButton": true,
    // true - show "Premium shop" button.
    "showPremiumShopButton": true,
    // true - show "Create squad" text on the squad creation button.
    "showCreateSquadButtonText": true,
    // true - show selected battle mode text.
    "showBattleTypeSelectorText": true,
    // true - show "Referral Program" button.
    "showReferralButton": true,
    // true - show "General chat" button.
    "showGeneralChatButton": true,
    // false - disable display promo of premium vehicle (on the background in the hangar).
    "showPromoPremVehicle": false,
    // true - show info windows with the battle results in the "Ranked battle" mode.
    "showRankedBattleResults": true,
    // true - show info windows when receiving progressive decals.
    "showProgressiveDecalsWindow": true,
    // true - show widget "Daily Quests" in the hangar.
    "showDailyQuestWidget": true,
    // true - show banner of various events in the hangar.
    "showEventBanner": true,
    // "Combat Intelligence" - show/hide notifications in the main window and counters in the menu.
    "combatIntelligence": {
      // false - disable display pop-up messages in the hangar.
      "showPopUpMessages": true,
      // false - disable display unread notifications counter in the menu.
      "showUnreadCounter": true
    },
    // Parameters of the "Session statistics" button.
    "sessionStatsButton": {
      // false - disable display "Session statistics" button.
      "showButton": true,
      // false - disable display the counter of spent battles on the button.
      "showBattleCount": true
    },
    // true - enable locker for gold.
    "enableGoldLocker": false,
    // true - enable locker for free XP.
    "enableFreeXpLocker": false,
    // true - enable locker for bonds.
    "enableCrystalLocker": false,
    // Show/hide server info or change its parameters.
    "serverInfo": {
      // Show server info in hangar.
      "enabled": true,
      // Transparency in percents [0..100].
      "alpha": 100,
      // Rotation in degrees [0..360].
      "rotation": 0,
      // X offset.
      "offsetX": 0,
      // Y offset.
      "offsetY": 0
    },
    // true - enable crew auto return function (the option works if there are free places in the barracks).
    "enableCrewAutoReturn": true,
    // true - return crew check box is selected by default.
    "crewReturnByDefault": false,
    // Number of perks to show without grouping.
    "crewMaxPerksCount": 8,
    // Show/hide common quests button or change its parameters.
    "commonQuests": {
      // Show common quests button in hangar.
      "enabled": true,
      // Transparency in percents [0..100].
      "alpha": 100,
      // Rotation in degrees [0..360].
      "rotation": 0,
      // X offset.
      "offsetX": 0,
      // Y offset.
      "offsetY": 0
    },
    // Show/hide personal quests button or change its parameters.
    "personalQuests": {
      // Show personal quests button in hangar.
      "enabled": true,
      // Transparency in percents [0..100].
      "alpha": 100,
      // Rotation in degrees [0..360].
      "rotation": 0,
      // X offset.
      "offsetX": 0,
      // Y offset.
      "offsetY": 0
    },
    // Show/hide current vehicle name, type and level or change their parameters.
    "vehicleName": {
      // Show current vehicle name, type and level in hangar.
      "enabled": true,
      // Transparency in percents [0..100].
      "alpha": 100,
      // Rotation in degrees [0..360].
      "rotation": 0,
      // X offset.
      "offsetX": 0,
      // Y offset.
      "offsetY": 0
    },
    // true - make vehicle not ready for battle if low ammo.
    "blockVehicleIfLowAmmo": true,
    // Below this percentage, ammo is low. (0 - 100).
    "lowAmmoPercentage": 50,
    // Behavior of the system channel notifications button on new notifications:
    //   none - do nothing
    //   blink - blink button
    //   full - blink and show counter (default client behavior)
    "notificationsButtonType": "full",
    // true - hide price button in tech tree.
    "hidePricesInTechTree": false,
    // true - show mastery mark in tech tree.
    "masteryMarkInTechTree": true,
    // true - allow to consider the exchange of experience with gold in tech tree.
    "allowExchangeXPInTechTree": true,
    // true - show flags in barracks.
    "barracksShowFlags": true,
    // true - show skills in barracks.
    "barracksShowSkills": true,
    // true - restore selected battle type on switching to another server, at the next login to the client.
    "restoreBattleType": false,
    // Ping servers.
    "pingServers": {
      // true - enable display of ping to the servers.
      "enabled": true,
      // Update interval, in ms.
      "updateInterval": 5000,
      // Axis field coordinates.
      "x": 80,
      "y": 69,
      // Horizontal alignment of field at screen ("left", "center", "right").
      "hAlign": "left",
      // Vertical alignment of field at screen ("top", "center", "bottom").
      "vAlign": "top",
      // Transparency (from 0 to 100).
      "alpha": 80,
      // If set, draw image at background.
      // example: "bgImage": "cfg://My/img/my.png",
      "bgImage": null,
      // Server to response time text delimiter.
      "delimiter": ": ",
      // Maximum number of column rows.
      "maxRows": 1,
      // Gap between columns.
      "columnGap": 3,
      // Leading between lines.
      "leading": 2,
      // Layer - "bottom", "normal" (default), "top".
      "layer": "normal",
      // true - show title "Ping".
      "showTitle": false,
      // true - show server names in list.
      "showServerName": true,
      // Expand server names to this amount of symbols. recommended to use monospace font if this option is set.
      "minimalNameLength": 4,
      // Expand values to this amount of symbols. recommended to use monospace font if this option is set.
      "minimalValueLength": 0,
      // Text to show in case of error.
      "errorString": "—",
      // List of ignored servers, for example, ["RU1", "RU3"].
      "ignoredServers": [],
      // Text style.
      "fontStyle": {
        // Font name.
        "name": "$FieldFont",
        "size": 12,
        "bold": false,
        "italic": false,
        // Different colors depending on server response time.
        "color": {
          "great": "0xFFCC66",
          "good":  "0xE5E4E1",
          "poor":  "0x96948F",
          "bad":   "0xD64D4D"
        },
        // Color for server name and delimiter (for example, "0x8080FF"). Empty string "" - use same color as online value.
        "serverColor": ""
      },
      // Text format for current server in the list,
      // Flash HTML tags supported. "{server}" for the server value.
      "currentServerFormat": "<b>{server}</b>",
      // Threshold values defining response quality.
      "threshold": {
        // Below this value response is great.
        "great": 35,
        // Below this value response is good.
        "good": 60,
        // Below this value response is poor.
        "poor": 100
        // Values above define bad response.
      },
      // Shadow options.
      "shadow": {
        // false - no shadow.
        "enabled": true,
        "distance": 0,       // (in pixels)    / offset distance
        "angle": 0,          // (0.0 .. 360.0) / offset angle
        "color": "0x000000", // "0xXXXXXX"     / color
        "alpha": 70,         // (0 .. 100)     / opacity
        "blur": 4,           // (0.0 .. 255.0) / blur
        "strength": 2        // (0.0 .. 255.0) / intensity
      }
    },
    "onlineServers": {
      // true - enable display online of servers.
      "enabled": false
    },
    // Show/hide notifications counters in the main menu.
    "notificationCounter": {
      "storage": true,                       // Storage
      "store": true,                         // Store
      "missions": true,                      // Missions
      "PersonalMissionOperationsPage": true, // Campaigns
      "profile": true,                       // Service Record
      "barracks": true,                      // Barracks
      "StrongholdView": true                 // Clan
    },
    // true - show notifications counters in the window and on the button "Exterior".
    "showCustomizationCounter": true,
    // Parameters of sorting tankmen in barracks.
    "barracks": {
      // Order of nations.
      //"nations_order": ["ussr", "germany", "usa", "china", "france", "uk", "japan", "czech", "poland", "sweden", "italy"],
      "nations_order": [],
      // Order of crew roles.
      // "roles_order": ["commander", "gunner", "driver", "radioman", "loader"],
      "roles_order": [],
      // Tankmen sorting criteria, available options: (minus = reverse order)
      // "nation", "role", "level", "-level", "XP", "-XP", "gender", "-gender", "inVehicle", "-inVehicle", "vehicle"
      "sorting_criteria": ["nation", "inVehicle", "vehicle", "role"]
    },
    // Parameters for tank carousel.
    "carousel": ${"carousel.xc":"carousel"},
    // Parameters for widgets.
    "widgets": ${"widgets.xc":"widgets.lobby"}
  }
}
