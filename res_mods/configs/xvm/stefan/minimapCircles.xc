﻿/**
 * Minimap circles. Only real map meters. Only for own unit.
 * https://kr.cm/f/t/15467/c/187139
 * https://kr.cm/f/t/15467/c/186794
 */
{
  "circles": {
    "view": [
      // Current spotting distance.
      { "enabled": true,  "distance": "blindarea", "scale": 1, "thickness": 0.5, "alpha": 60, "color": "0x00FF00" },
      // Maximum spotting distance.
      { "enabled": true,  "distance": 445,         "scale": 1, "thickness": 0.5, "alpha": 60, "color": "0xFFFFFF" },
      // Maximum drawing distance.
      { "enabled": "{{my-vtype-key=SPG?false|true}}", "distance": 564, "scale": 1, "thickness": 0.5, "alpha": 60, "color": "0xFFFF00" },
      // 50m proximity spotting.
      { "enabled": true,  "distance": 50,          "scale": 1, "thickness": 0.6, "alpha": 60, "color": "0x77FFFF" },
      // Maximum spotting distance with binocs (might be > max. spotting distance).
      { "enabled": true,  "distance": "standing",  "scale": 1, "thickness": 0.5, "alpha": 60, "color": "0x00BB00" },
      { "enabled": false, "distance": "motion",    "scale": 1, "thickness": 0.5, "alpha": 60, "color": "0x0000FF" },
      { "enabled": false, "distance": "dynamic",   "scale": 1, "thickness": 0.5, "alpha": 60, "color": "0x3EB5F1" }
    ],
    // Artillery range.
    // Maximum range of fire for artillery Artillery gun fire range may differ
    // depending on vehicle angle relative to ground and vehicle height
    // positioning relative to target. These factors are not considered.
    // See pics at https://kr.cm/f/t/2076/c/35697/
    "artillery": { "enabled": true, "alpha": 55, "color": "0xFF6666", "thickness": 0.5 },
    // Range for low caliber rounds
    "shell":     { "enabled": true, "alpha": 55, "color": "0xFF6666", "thickness": 0.5 }
  }
}
