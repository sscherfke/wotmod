﻿/**
 * Widgets templates.
 */
{
  "clock": {
    // Show clock in hangar.
    "enabled": true,
    // layer - "bottom", "normal" (default), "top".
    "layer": "normal",
    "type": "extrafield",
    "formats": [
      {
        // Background image.
        "x": 4,
        "y": 51,
        "screenHAlign": "right",
        "format": "<img src='xvm://res/icons/clock/clockBg.png'>"
      },
      {
        "updateEvent": "ON_EVERY_SECOND",
        // Horizontal position.
        "x": -10,
        // Vertical position.
        "y": 38,
        // Width.
        "width": 200,
        // Height.
        "height": 50,
        // Horizontal alignment of field at screen ("left", "center", "right").
        "screenHAlign": "right",
        "shadow": {
          // false - no shadow.
          "enabled": true,
          "distance": 0,       // (in pixels)    / offset distance
          "angle": 0,          // (0.0 .. 360.0) / offset angle
          "color": "0x000000", // "0xXXXXXX"     / color
          "alpha": 70,         // (0 .. 100)     / opacity
          "blur": 4,           // (0.0 .. 255.0) / blur
          "strength": 2        // (0.0 .. 255.0) / intensity
        },
        "textFormat": { "align": "right", "valign": "bottom", "color": "0x959688" },
        "format": "<font face='$FieldFont'><textformat leading='-38'><font size='36'>{{py:xvm.formatDate('%H:%M')}}</font><br></textformat><textformat rightMargin='85' leading='-2'>{{py:xvm.formatDate('%A')}}<br><font size='15'>{{py:xvm.formatDate('%d %b %Y')}}</font></textformat></font>"
      }
    ]
  }
}
