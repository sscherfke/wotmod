﻿/**
 * Text substitutions.
 */
{
  "texts": {
    // Text for {{vtype}} macro.
    "vtype": {
      // Text for light tanks.
      "LT":  "{{l10n:LT}}",
      // Text for medium tanks.
      "MT":  "{{l10n:MT}}",
      // Text for heavy tanks.
      "HT":  "{{l10n:HT}}",
      // Text for SPG.
      "SPG": "{{l10n:SPG}}",
      // Text for tank destroyers.
      "TD":  "{{l10n:TD}}"
    },
    // Text for {{marksOnGun}}, {{v.marksOnGun}} macros.
    "marksOnGun": {
      "_0": "0",
      "_1": "1",
      "_2": "2",
      "_3": "3"
    },
    // Text for {{spotted}} macro.
    "spotted": {
      "neverSeen": "",
      "lost": "<font face='xvm' size='24'>&#x70;</font>",
      "spotted": "<font face='xvm' size='24'>&#x70;</font>",
      "dead": "",
      "neverSeen_arty": "",
      "lost_arty": "<font face='xvm' size='24'>&#x70;</font>",
      "spotted_arty": "<font face='xvm' size='24'>&#x70;</font>",
      "dead_arty": ""
    },
    // Text for {{xvm-user}} macro.
    "xvmuser": {
      // XVM with enabled statistics.
      "on": "on",
      // XVM with disabled statistics.
      "off": "off",
      // Without XVM.
      "none": "none"
    },
    // Text for {{battletype}} macro.
    "battletype": {
      // Unknown battle.
      "unknown": "",
      // Random battle.
      "regular": "",
      "training": "training",
      // Tournament.
      "tournament": "tournament",
      // Clan wars.
      "clan": "clan",
      // Battle training.
      "tutorial": "",
      // Team battles.
      "cybersport": "cybersport",
      // Special game mode (racing, football and other).
      "event_battles": "",
      // Global map (GM).
      "global_map": "global_map",
      // Regular tournament (event) GM.
      "tournament_regular": "tournament_regular",
      // Periodic Tournament (event) GM.
      "tournament_clan": "tournament_clan",
      // Sandbox PVE, <10 battles.
      "rated_sandbox": "",
      // Sandbox PVE, >10 battles.
      "sandbox": "",
      // Steel Hunt.
      "fallout_classic": "",
      // Domination.
      "fallout_multiteam": "",
      // Strongholds, skirmish.
      "sortie_2": "sortie_2",
      // Strongholds, advance.
      "fort_battle_2": "fort_battle_2",
      // Ranked battle.
      "ranked": "",
      // Proving ground.
      "bootcamp": "",
      // Grand battles.
      "epic_random": "",
      // Grand battles (training).
      "epic_random_training": "",
      // Special game mode (new)(racing, football and other).
      "event_battles_2": "",
      // Frontline.
      "epic_battle": "",
      // Frontline (training).
      "epic_battle_training": "",
      // Steel Hunter (solo).
      "battle_royale_solo": "",
      // Steel Hunter (squad).
      "battle_royale_squad": "",
      // Tournament (event).
      "tournament_event": "",
      // ?.
      "event_random": "",
      // «Team Clash».
      "bob": "",
      // Steel Hunter (solo, training).
      "battle_royale_training_solo": "",
      // Steel Hunter (squad, training).
      "battle_royale_training_squad": "",
      // «Brawl» mode.
      "weekend_brawl": ""
    },
    // Text for {{topclan}} macro.
    "topclan": {
      "top": "top",
      "persist": "persist",
      "regular": ""
    }
  }
}
