﻿/**
 * Various settings for advanced users.
 */
{
  "tweaks": {
    // !!!ATTENTION!!! re-read the license agreement carefully before enabling this option.
    // Running two, three, or more client instances requires the use of multiple accounts.
    //
    // Europe        : http://legal.eu.wargaming.net/en/eula/
    //
    // true - allows to run additional  WoT instances.
    "allowMultipleWotInstances": false
  }
}
