"""
Read WoT's binary "XML" (it's not actual XML ...) and return it as dict.

Ported from https://bitbucket.org/rstarkov/wotdatalib/src/tip/BxmlReader.cs

You can also directly invoke this script with an XML filename.  It will print
its parsed contents as JSON::

    $ python bxml.py path_to_script.xml
    {
        ...
    }

"""
import base64
import collections
import enum
import json


__all__ = ['read_file']


Child = collections.namedtuple('Child', 'name, length, type')


class Type(enum.Enum):
    """Child data types."""
    DICT = 0
    STRING = 1
    INT = 2
    FLOAT = 3
    BOOL = 4
    BASE64 = 5


def read_file(reader):
    """Read WoT XML from *reader* (a binary stream, e.g., an open file) and
    return a dict.

    """
    if read_int32(reader) != 0x62A14E45:
        raise ValueError('This file does not look like a valid binary-xml file')

    # Read a 0 byte - not sure what it's purpose is
    assert reader.read(1) == b'\x00'

    # Read an array strings.  These strings are used for the dictionary keys
    # when reading dictionaries (they are never encoded "in-line"; all
    # dictionary keys are indices into this list of strings).
    strings = []
    while True:
        string = read_string(reader)
        if not string:
            break
        strings.append(string)

    return read_dict(reader, strings)


def read_dict(reader, strings):
    """Reads a value of type dictionary from the current position of the binary
    reader.

    Note that in addition to a number of children, a dictionary may also have
    an "own" value, which is never a dictionary, and which this function stores
    at the empty key (``result[""]``).

    *reader* is the open file objected.

    *strings* is the list of strings read from the start of the file, used as
    dictionary keys.

    """
    # Read the number of children that this dictionary has
    child_count = read_int16(reader)

    # Read the length and data type of the "own" value.
    end_and_type = read_int32(reader)
    own_value_length = end_and_type & 0x0fffffff
    own_value_type = Type(end_and_type >> 28)
    if own_value_type == Type.DICT:
        raise ValueError('14516')  # Wat?

    prev_end = own_value_length

    # Read information about each of the child values: the key name, the length and type of the value
    children = []
    # for i in range(child_count+1):
    for _ in range(child_count):
        name = strings[read_int16(reader)]
        end_and_type = read_int32(reader)
        end = end_and_type & 0x0fffffff
        length = end - prev_end
        prev_end = end
        children.append(Child(name, length, Type(end_and_type >> 28)))

    # Read the own value
    result = {}
    if (own_value_length > 0) or (own_value_type is not Type.STRING):
        # some dictionaries don't have an "own" value:
        result[""] = read_data(reader, strings, own_value_type, own_value_length)

    # Read the child values
    for child in children:
        result[child.name] = read_data(reader, strings, child.type, child.length)

    return result

def read_data(reader, strings, type_, length):
    """Reads a value from the current position of the binary reader.  The value
    may be a nested dictionary, or one of the primitive types.

    *reader* is the open file objected.

    *strings* is the list of strings read from the start of the file, used as
    dictionary keys.

    *type_* is the type of the value (which must have been read elsewhere in
    the binary data file).

    *length* is the length of the value (which must have been read elsewhere in
    the binary data file).

    """
    if type_ is Type.DICT:
        return read_dict(reader, strings)
    elif type_ is Type.STRING:
        return reader.read(length).decode()
    elif type_ is Type.INT:
        if length == 0:
            return 0
        elif length == 1:
            return read_int8(reader)
        elif length == 2:
            return read_int16(reader)
        elif length == 4:
            return read_int32(reader)
        else:
            raise ValueError('Unexpected length for Int')
    elif type_ is Type.FLOAT:
        floats = []
        for i in range(length):
            floats.append(read_int8(reader))
        if len(floats) == 1:
            return floats[0]
        else:
            return floats
    elif type_ is Type.BOOL:
        value = (length == 1)
        # Only read if "length == 1"!
        if value and reader.read(1) != b'\x01':
            raise ValueError('Boolean error')
        return value
    elif type_.BASE64:
        val = reader.read(length)
        try:
            return base64.b64decode(val).decode()
        except:
            return f'broken base64: {val}'
    else:
        raise ValueError('Unknown type')


def read_int8(reader):
    """Read and return a signed little-endian int16 from *reader*."""
    return int.from_bytes(reader.read(1), byteorder='little')


def read_int16(reader):
    """Read and return a signed little-endian int16 from *reader*."""
    return int.from_bytes(reader.read(2), byteorder='little')


def read_int32(reader):
    """Read and return a signed little-endian int32 from *reader*."""
    return int.from_bytes(reader.read(4), byteorder='little')


def read_string(reader):
    """Read and return a null byte delimted string from *reader*."""
    buf = bytearray()
    while True:
        b = reader.read(1)
        if b == b'\x00':
            break
        buf.extend(b)
    return buf.decode()


if __name__ == '__main__':
    import json
    import sys

    filename = sys.argv[1]
    data = read_file(open(filename, 'rb'))
    print(json.dumps(data, indent=4))
