#!/usr/bin/env python3
"""
Rename the [res_]mods folders for a new WoT version and make a commit.

Usage:
  ./update_wot.py

"""
import fnmatch
import functools
import os
import subprocess


WOT_VER_OLD, WOT_VER_NEW = sorted(fnmatch.filter(os.listdir('mods/'), '1.*'))

answer = input(f'Update WoT {WOT_VER_OLD} to {WOT_VER_NEW}? [Y/n]: ')
if answer == 'n':
    exit()

run = functools.partial(subprocess.run, shell=True, check=True)

for dirname in ['mods', 'res_mods']:
    run(f'git checkout {dirname}/{WOT_VER_OLD}')
    run(f'rm -rf {dirname}/{WOT_VER_NEW}')
    run(f'git mv {dirname}/{WOT_VER_OLD} {dirname}/{WOT_VER_NEW}')

run('git status')
commit = f"git commit -m 'WoT {WOT_VER_NEW}'"
answer = input(f'Commit changes? [Y/n] ')
if answer == 'n':
    print(f'Please commit when you are done:\n{commit}')
    exit()
run(commit)
